package com.sparkdigital.contest;

import com.sparkdigital.contest.handlers.CpuHandler;
import com.sparkdigital.contest.handlers.DefaultHandler;
import com.sparkdigital.contest.handlers.IOHandler;
import com.sparkdigital.contest.handlers.MemoryHandler;
import com.sparkdigital.contest.handlers.ThroughputHandler;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

public class Server {

    private final int port;

    public Server(int port) {
        this.port = port;
    }

    public void run() throws Exception {
        // Configure the server.
        EventLoopGroup bossGroup = new NioEventLoopGroup(20);
        EventLoopGroup workerGroup = new NioEventLoopGroup(20);
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.option(ChannelOption.SO_BACKLOG, 1024);
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {

                        @Override
                        protected void initChannel(SocketChannel ch)
                                throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast("codec", new HttpServerCodec());
                            p.addLast("throughput", ThroughputHandler.INSTANCE);
                            p.addLast("cpu", CpuHandler.INSTANCE);
                            p.addLast("ram", MemoryHandler.INSTANCE);
                            p.addLast("disk", IOHandler.INSTANCE);
                            p.addLast("default", DefaultHandler.INSTANCE);
                        }

                    });

            Channel ch = b.bind(port).sync().channel();
            ch.closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        int port;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        } else {
            port = 8080;
        }
        new Server(port).run();
    }
}
