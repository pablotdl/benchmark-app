package com.sparkdigital.contest.handlers;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import com.sparkdigital.contest.support.FileLoader;
import com.sparkdigital.contest.support.FileLoaderFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

@Sharable
public class ThroughputHandler extends BaseHandler {

	public static final ThroughputHandler INSTANCE = new ThroughputHandler();

	private ByteBuf res;
	private DefaultFullHttpResponse response;

	public ThroughputHandler() {
		super("/throughput");
		FileLoader loader = FileLoaderFactory.loader();
		try (InputStream is = loader.loadStream("/throughput.json")) {
			byte[] content = IOUtils.toByteArray(is);
			res = Unpooled.unreleasableBuffer(Unpooled.buffer(content.length));
			res.writeBytes(content);
			response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, res);
			response.headers().add("Content-type", "application/json");
			response.headers().add("Content-length", content.length);
		} catch (IOException ioe) {
			throw new IllegalStateException("Unable to read response", ioe);
		}
	}

	@Override
	public void doRead(ChannelHandlerContext ctx, Object msg) {
		response.content().resetReaderIndex();
		ctx.writeAndFlush(response);
		ctx.close();
	}

	
}
