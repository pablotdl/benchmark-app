package com.sparkdigital.contest.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultHttpRequest;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;

public abstract class BaseHandler extends ChannelInboundHandlerAdapter {

	private final String baseUri;

	public BaseHandler(String baseUri) {
		this.baseUri = baseUri;
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		if (msg == LastHttpContent.EMPTY_LAST_CONTENT) {
			ctx.writeAndFlush(new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK));
			ctx.close();
			return;
		}
		if (!(msg instanceof DefaultHttpRequest)) {
			super.channelRead(ctx, msg);
			return;
		}
		DefaultHttpRequest request = (DefaultHttpRequest) msg;
		if (!request.uri().startsWith(baseUri)) {
			super.channelRead(ctx, msg);
			return;
		}
		doRead(ctx, msg);
	}

	protected abstract void doRead(ChannelHandlerContext ctx, Object msg) throws Exception;
}
