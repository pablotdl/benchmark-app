package com.sparkdigital.contest.handlers;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import com.sparkdigital.contest.support.FileLoader;
import com.sparkdigital.contest.support.FileLoaderFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

@Sharable
public class IOHandler extends BaseHandler {

	public static final IOHandler INSTANCE = new IOHandler();

	private final FileLoader loader;

	public IOHandler() {
		super("/disk");
		this.loader = FileLoaderFactory.loader();
	}

	@Override
	protected void doRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		File temp = File.createTempFile("test", ".csv");
		long bytes = copyFile(temp);
		ByteBuf result = Unpooled.wrappedBuffer(("{\"bytes\": " + bytes + "}").getBytes());
		ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, result));
		ctx.close();
		Files.delete(temp.toPath());
	}

	private long copyFile(File temp) throws Exception {
		Path source = loader.loadPath("/disk_test.csv");
		//Paths.get(getClass().getResource("/disk_test.csv").toURI());
		Path dest = temp.toPath();
		Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
		return Files.size(dest);
	}

}
