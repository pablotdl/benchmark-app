package com.sparkdigital.contest.handlers;

import java.security.MessageDigest;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

@Sharable
public class CpuHandler extends BaseHandler {

	public static final CpuHandler INSTANCE = new CpuHandler();

	private byte[] seed = "Sparkers doing some benchmarking".getBytes();
	private byte[] prefix = "{\"Hashed\": \"".getBytes();
	private byte[] sufix = "\"}".getBytes();
	private byte[] hexas = "0123456789ABCDEF".getBytes();

	public CpuHandler() {
		super("/cpu");
	}

	@Override
	protected void doRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		MessageDigest digest = MessageDigest.getInstance("SHA-512");
		byte[] hashed = seed;
		for (int i = 0; i < 256; i++) {
			hashed = digest.digest(hashed);
		}
		byte[] result = new byte[78];
		System.arraycopy(prefix, 0, result, 0, prefix.length);
		for (int i = 0; i < hashed.length; i++) {
			result[prefix.length + i] = hexas[hashed[i] & 0xFF >>> 4];
		}
		System.arraycopy(sufix, 0, result, prefix.length + hashed.length, sufix.length);
		DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,
				Unpooled.wrappedBuffer(result));
		ctx.writeAndFlush(response);
		ctx.close();
	}

}
