package com.sparkdigital.contest.handlers;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;

import com.sparkdigital.contest.support.FileLoaderFactory;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;

@Sharable
public class MemoryHandler extends BaseHandler {

	public static final MemoryHandler INSTANCE = new MemoryHandler();

	private ForkJoinPool pool = ForkJoinPool.commonPool();

	private final String text;

	public MemoryHandler() {
		super("/ram");
		try (InputStream is = FileLoaderFactory.loader().loadStream("/ram_test.txt")) {
			text = IOUtils.toString(is);
		} catch (IOException ioe) {
			throw new IllegalStateException("Error loading file", ioe);
		}
	}

	@Override
	protected void doRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		int vowels = countVowels(text);
		String result = "{ \"n_vowels\": " + vowels + " }";
		DefaultFullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK,
				Unpooled.wrappedBuffer(result.getBytes()));
		ctx.writeAndFlush(response);
		ctx.close();
	}

	protected int countVowels(String text) {
		return pool.invoke(new VowelCountingTask(text));
	}

	public static class VowelCountingTask extends RecursiveTask<Integer> {

		private static final long serialVersionUID = 8227462092276781415L;
		private static final Pattern VOWELS = Pattern.compile("[aeiou]");
		private static final int THRESHOLD = 200;

		private final StringBuilder sb;
		private final int start;
		private final int end;

		public VowelCountingTask(String text) {
			this(new StringBuilder(text), 0, text.length());
		}

		public VowelCountingTask(StringBuilder sb, int start, int end) {
			this.sb = sb;
			this.start = start;
			this.end = end;
		}


		@Override
		protected Integer compute() {
			if (this.end - this.start < THRESHOLD) {
				Matcher matcher = VOWELS.matcher(this.sb.subSequence(start, end));
				int count = 0;
				while (matcher.find()) {
					count++;
				}
				return count;
			}
			VowelCountingTask left = createTask(start, start + ((end - start) / 2));
			left.fork();
			VowelCountingTask right = createTask(start + ((end - start) / 2 + 1), end);
			return right.compute() + left.join();
		}

		protected VowelCountingTask createTask(int start, int end) {
			return new VowelCountingTask(sb, start, end);
		}

	}

}
