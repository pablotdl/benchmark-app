package com.sparkdigital.contest.support;

import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ClasspathFileLoader implements FileLoader {

	@Override
	public Path loadPath(String path) {
		try {
			URI uri = getClass().getResource(path).toURI();
			return Paths.get(uri);
		} catch (Exception e) {
			throw new IllegalArgumentException("Cannot load path " + path, e);
		}
	}

	@Override
	public InputStream loadStream(String path) {
		return getClass().getResourceAsStream(path);
	}

}
