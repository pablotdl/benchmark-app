package com.sparkdigital.contest.support;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FilesystemFileLoader implements FileLoader {

	private final Path base;

	public FilesystemFileLoader(String base) {
		this.base = Paths.get(base);
	}

	@Override
	public Path loadPath(String path) {
		return this.base.resolve(path);
	}

	@Override
	public InputStream loadStream(String path) {
		try {
			return Files.newInputStream(this.base.resolve(path), StandardOpenOption.READ);
		} catch (IOException e) {
			throw new IllegalArgumentException("Cannot open path " + path, e);
		}
	}

}
