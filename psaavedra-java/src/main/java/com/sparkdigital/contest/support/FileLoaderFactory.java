package com.sparkdigital.contest.support;

public final class FileLoaderFactory {

	private static FileLoader instance = null;

	private FileLoaderFactory(){}

	public static FileLoader loader() {
		if (instance != null) {
			return instance;
		}
		String basePath = System.getProperty("basePath");
		if (basePath == null) {
			instance = new ClasspathFileLoader();
		} else {
			instance = new FilesystemFileLoader(basePath);
		}
		return instance;
	}
}
