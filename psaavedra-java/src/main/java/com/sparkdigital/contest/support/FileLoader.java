package com.sparkdigital.contest.support;

import java.io.InputStream;
import java.nio.file.Path;

public interface FileLoader {

	Path loadPath(String path);

	InputStream loadStream(String path);
}
