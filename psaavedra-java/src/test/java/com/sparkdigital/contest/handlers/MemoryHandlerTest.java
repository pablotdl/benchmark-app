package com.sparkdigital.contest.handlers;

import java.util.concurrent.ForkJoinPool;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.sparkdigital.contest.handlers.MemoryHandler.VowelCountingTask;

import junit.framework.Assert;

public class MemoryHandlerTest {

	@Test
	public void countVowelsForkTest() {
		String text = "0123456789";
		VowelCountingTask task = new VowelCountingTask(text);
		task.compute();
	}

	@Test
	public void countVowelsForkOddTest() {
		String text = "01234567897";
		VowelCountingTask task = new VowelCountingTask(text);
		task.compute();
	}

	@Test
	public void countVowelsTest() {
		String text = "abcdefghi";
		VowelCountingTask task = new VowelCountingTask(text);
		Assert.assertEquals(3, task.compute().intValue());
	}

	@Test
	public void properTest() throws Exception {
		ForkJoinPool pool = ForkJoinPool.commonPool();
		String text = IOUtils.toString(getClass().getResource("/ram_test.txt"));
		VowelCountingTask task = new VowelCountingTask(text);
		int result = pool.invoke(task);
		Assert.assertEquals(3716, result);
	}
}
