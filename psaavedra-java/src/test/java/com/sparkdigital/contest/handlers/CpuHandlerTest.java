package com.sparkdigital.contest.handlers;

import java.security.MessageDigest;

import org.junit.Test;

public class CpuHandlerTest {

	private byte[] seed = "Sparkers doing some benchmarking".getBytes();
	private byte[] prefix = "{\"Hashed\": \"".getBytes();
	private byte[] sufix = "\"}".getBytes();
	private byte[] hexas = "0123456789ABCDEF".getBytes();

	@Test
	public void digestToByteArray() throws Exception {
		MessageDigest digest = MessageDigest.getInstance("SHA-512");
		byte[] hashed = seed;
		for (int i = 0; i < 256; i++) {
			hashed = digest.digest(hashed);
		}
		byte[] result = new byte[78];
		System.arraycopy(prefix, 0, result, 0, prefix.length);
		//System.arraycopy(hashed, 0, result, prefix.length, hashed.length);
		for (int i = 0; i < hashed.length; i++) {
			result[prefix.length + i] = hexas[hashed[i] & 0xFF >>> 4];
		}
		
		System.arraycopy(sufix, 0, result, prefix.length + hashed.length, sufix.length);
	}
}
